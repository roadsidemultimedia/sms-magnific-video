<?php
/*
Plugin Name: SMS Magnific Video
Plugin URI: http://www.roadsidemultimedia.com
Description: Magnific Youtube Popup Video
Author: Curtis Grant
PageLines: true
Version: 1.0
Section: true
Class Name: SMSMagnificVideo
Filter: component
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-magnific-video
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;

global $mv_num;
$mv_num = 0;

class SMSMagnificVideo extends PageLinesSection {

	function section_styles(){
    
    wp_enqueue_script( 'magnific', $this->base_url.'/js/jquery.magnific-popup.js', array( 'jquery' ), pl_get_cache_key(), true );
    wp_enqueue_style( 'magnific-gform', $this->base_url.'/css/magnific-popup.css');
    wp_enqueue_style( 'magnific-video', $this->base_url.'/css/magnific-video.css');

  	}
	function section_opts(){


		$options = array();
		
		$options[] = array(
				'type'	=> 'multi',
				'key'	=> 'config', 
				'title'	=> 'Config',
				'col'	=> 1,
				'opts'	=> array(
					array(
						'type'	=> 'text',
						'key'	=> 'mv_link', 
						'label'	=> 'Video Embed Link',
						'help'  => __( 'ex. //www.youtube.com/embed/1XsUjY5Z8fk or //player.vimeo.com/video/1313493', 'pagelines' )
					),
					array(
						'type'	=> 'image_upload',
						'key'	=> 'mv_img', 
						'label'	=> 'Youtube Preview Image',
					),
				)
				
			);
		
		return $options;

	}


	function section_template( ) { 
	
		global $mv_num;
   			if(!$mv_num) {
     			$mv_num = 1;
   			}		

		$mvimg = ($this->opt('mv_img')) ? $this->opt('mv_img') : '';
		$mvlink = ($this->opt('mv_link')) ? $this->opt('mv_link') : '';


		?>
	<script type="text/javascript">
		jQuery(function($) {
    		$(document).ready(function() {
				$('.video-popup#<?php echo $mv_num; ?>').magnificPopup({
  					items: {
      				src: '<div class="white-popup magnific-video"><div class="video-container"><iframe width="100%" height="100%" src="<?php echo $mvlink; ?>?autoplay=1&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe></div></div>',
      				type: 'inline'
  					},
  					closeBtnInside: true
		});
      });
	});
	</script>
	<div class="video-popup" id="<?php echo $mv_num; ?>">
		<a class="video" href="javascript:;">
			<img src="<?php echo $mvimg; ?>" id="popup-img">
		</a>
		<div id="play">
			<i class="fa fa-play"></i>
		</div>
    </div>
			

<?php 
$mv_num++;
}

}